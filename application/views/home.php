<section  class="half-section main-section h-charter swip">
               <div class="container-fluid">
                  <div class="row m-0">
                     <div class="col-sm-12">
                        <div class="thm-h text-center">
                           <h2 class="wow fadeInUp" data-wow-duration="0.7s">Select a Yacht Fleet</h2>
                           <h4>tincidunt eget turpis non</h4>
                        </div>
                        <div class="owl-carousel charter-carousel  owl-theme">
                           <div class="item" data-dot="<h4>Single charter</h4><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eros velit, ullamcorper ut iaculis , dui felis, rutrum ac dolor non, porta volutpat quam. Mauris orci nibh, vehicula se dui felis, rutrum ac dolor non, porta volutpat quam. Mauris orci nibh, vehicula se</p>">  
                              <a  href="home-1.html#">
                              <img  src="assets/images/product_1.jpg">
                              </a>
                           </div>
                           <div class="item" data-dot="<h4>Elan 1923 Impression 2</h4><p>Adipiscing elit. Nam eros velit, ullamcorper ut iaculis , dui felis, rutrum ac dolor non, porta volutpat quam. Mauris orci nibh, Lorem ipsum dolor sit amet, consectetur</p>">  
                              <a  href="home-1.html#">
                              <img src="assets/images/product_2.jpg">
                              </a>
                           </div>
                           <div class="item" data-dot="<h4>Elan 1923 Impression 3</h4><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eros velit, ullamcorper ut iaculis ,Lorem ipsum dolor serrfs , porta volutpat quam. Mauris orci nibh, vehicula se</p>">  
                              <a  href="home-1.html#">
                              <img src="assets/images/product_3.jpg">
                              </a>
                           </div>
                           <div class="item" data-dot="<h4>Elan 1923 Impression 4</h4><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eros velit, ullamcorper ut iaculis , dui felis, rutrum ac dolor non, porta volutpat quam. Mauris orci nibh, vehicula se dui felis, rutrum ac dolor non, porta volutpat quam. Mauris orci nibh, vehicula se</p>">  
                              <a  href="home-1.html#">
                              <img src="assets/images/product_4.jpg">
                              </a>
                           </div>
                           <div class="item" data-dot="<h4>Elan 1923 Impression 5</h4><p>tempus erat nec auctor tincidunt. Suspendisse tempus malesuada ornare. Proin non egestas justo.</p>">  
                              <a  href="home-1.html#">
                              <img src="assets/images/product_5.jpg">
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section id="h-our-history-2" class="half-section main-section h-our-history-3   swip">
               <div class="container-fluid">
                  <div class="row ">
                     <div class="col-sm-6 half-left jumbotron" style="background: url('assets/images/our-history-2.jpg') no-repeat center;
                        background-size: cover;"
                        data-paroller-factor="-0.3"
                        data-paroller-factor-xs="0.2" >
                        <!--  <img class="img-fluid" src="assets/images/our-history-2.jpg"> -->
                     </div>
                     <div class="col-sm-6 half-right">
                        <div class="writing-sec my-paroller" data-paroller-factor="0.3" data-paroller-type="foreground" data-paroller-direction="horizontal">
                           <div class="thm-h">
                              <h2 class="wow fadeInUp" data-wow-duration="0.7s">Our history</h2>
                              <h4>tincidunt eget turpis non</h4>
                           </div>
                           <p class="wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">Owning a superyacht is the freedom to explore the world on your terms without limits. With 45 years of experience sourcing the best yachts at the best prices, YPI’s team of worldwide brokers are experts in finding the right yacht for every client. 
                           </p>
                           <a href="home-1.html#" class="read-more button-fancy -color-1 wow fadeInUp" data-wow-duration="1s"><span class="btn-arrow"></span><span class="twp-read-more text">Continue Reading</span></a>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section id="h-buy" class="half-section main-section swip">
               <div class="container-fluid">
                  <div class="row m-0">
                     <div class="col-sm-5 half-left">
                        <div class="img-sec  mega my-element"
                           data-paroller-factor="0.05"
                           data-paroller-factor-lg="0.4"
                           data-paroller-factor-md="0.3"
                           data-paroller-factor-sm="-0.2"
                           data-paroller-factor-xs="-0.1"
                           data-paroller-type="foreground"
                           data-paroller-direction="horizontal" >
                           <img class="img-fluid" src="assets/images/2.jpg" >
                        </div>
                     </div>
                     <div class="col-sm-7 half-right">
                        <div class="writing-sec mega my-element"
                           data-paroller-factor="-0.2"
                           data-paroller-factor-lg="0.4"
                           data-paroller-factor-md="0.3"
                           data-paroller-factor-sm="-0.2"
                           data-paroller-factor-xs="-0.1"
                           data-paroller-type="foreground"
                           data-paroller-direction="horizontal">
                           <div class="thm-h">
                              <h2 class="wow fadeInUp" data-wow-duration="0.7s">BUY WITH YPI</h2>
                              <h4>tincidunt eget turpis non</h4>
                           </div>
                           <p class="wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">Owning a superyacht is the freedom to explore the world on your terms without limits. With 45 years of experience sourcing the best yachts at the best prices, YPI’s team of worldwide brokers are experts in finding the right yacht for every client. 
                           </p>
                           <a href="home-1.html#" class="read-more button-fancy -color-1 wow fadeInUp" data-wow-duration="1s"><span class="btn-arrow"></span><span class="twp-read-more text">Continue Reading</span></a>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section id="h-sell" class="half-section main-section swip">
               <div class="container-fluid">
                  <div class="row m-0 rtl-sec">
                     <div class="col-sm-5 half-right">
                        <div class="img-sec  mega my-element"
                           data-paroller-factor="0.03"
                           data-paroller-factor-lg="0.4"
                           data-paroller-factor-md="0.3"
                           data-paroller-factor-sm="-0.2"
                           data-paroller-factor-xs="-0.1"
                           data-paroller-type="foreground"
                           data-paroller-direction="horizontal">
                           <img class="img-fluid" src="assets/images/4.jpg">
                        </div>
                     </div>
                     <div class="col-sm-7 half-left ">
                        <div class="writing-sec mega my-element"
                           data-paroller-factor="-0.05"
                           data-paroller-factor-lg="0.4"
                           data-paroller-factor-md="0.3"
                           data-paroller-factor-sm="-0.2"
                           data-paroller-factor-xs="-0.1"
                           data-paroller-type="foreground"
                           data-paroller-direction="horizontal" >
                           <div class="thm-h">
                              <h2>SELL YOUR YACHT</h2>
                              <h4>tincidunt eget turpis non</h4>
                           </div>
                           <p>Owning a superyacht is the freedom to explore the world on your terms without limits. With 45 years of experience sourcing the best yachts at the best prices, YPI’s team of worldwide brokers are experts in finding the right yacht for every client. 
                           </p>
                           <a href="home-1.html#" class="read-more button-fancy -color-1"><span class="btn-arrow"></span><span class="twp-read-more text">Continue Reading</span></a>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            
            <section id="h-charter" class="main-section h-charter-sec swip ">
               <div class="container-fluid">
                  <div class="row m-0 ">
                     <div class="thm-h sec-main-h">
                        <h2 class="wow fadeInUp" data-wow-duration="1s" >Our Charter Destinations </h2>
                     </div>
                     <div class="col-sm-12"  >
                        <div class="owl-carousel owl-carousel-3 center-carousel owl-theme">
                           <div class="item">
                              <a  href="home-1.html#">
                                 <div class="main-tilt">
                                    <div>
                                       <img class="main-back" src="assets/images/c-1.jpg" alt="">
                                    </div>
                                    <div   class="inner-tilt">
                                       <img class="inner-img" src="assets/images/c-1.jpg" alt="">
                                       <div class="slide-cont">
                                          <div>
                                             <h3>MALDIVES
                                             </h3>
                                             <p >Indian Ocean &amp; South East Asia</p>
                                             <span>View itinerary
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <div class="item">
                              <a  href="home-1.html#">
                                 <div class="main-tilt">
                                    <div data-speed="0.2">
                                       <img class="main-back" src="assets/images/c-2.jpg" alt="">
                                    </div>
                                    <div data-speed="0.5" class="inner-tilt">
                                       <img class="inner-img" src="assets/images/c-2.jpg" alt="">
                                       <div class="slide-cont">
                                          <div>
                                             <h3>MALDIVES
                                             </h3>
                                             <p >Indian Ocean &amp; South East Asia</p>
                                             <span>View itinerary
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <div class="item">
                              <a  href="home-1.html#">
                                 <div class="main-tilt">
                                    <img class="main-back" src="assets/images/c-3.jpg" alt="">
                                    <div class="inner-tilt">
                                       <img class="inner-img" src="assets/images/c-3.jpg" alt="">
                                       <div class="slide-cont">
                                          <div>
                                             <h3>MALDIVES
                                             </h3>
                                             <p >Indian Ocean &amp; South East Asia</p>
                                             <span>View itinerary
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <div class="item">
                              <a  href="home-1.html#">
                                 <div class="main-tilt">
                                    <img class="main-back" src="assets/images/c-4.jpg" alt="">
                                    <div class="inner-tilt">
                                       <img class="inner-img" src="assets/images/c-4.jpg" alt="">
                                       <div class="slide-cont">
                                          <div>
                                             <h3>MALDIVES
                                             </h3>
                                             <p >Indian Ocean &amp; South East Asia</p>
                                             <span>View itinerary
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <div class="item">
                              <a  href="home-1.html#">
                                 <div class="main-tilt">
                                    <img class="main-back" src="assets/images/c-5.jpg" alt="">
                                    <div class="inner-tilt">
                                       <img class="inner-img" src="assets/images/c-5.jpg" alt="">
                                       <div class="slide-cont">
                                          <div>
                                             <h3>MALDIVES
                                             </h3>
                                             <p >Indian Ocean &amp; South East Asia</p>
                                             <span>View itinerary
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <div class="item">
                              <a  href="home-1.html#">
                                 <div class="main-tilt">
                                    <img class="main-back" src="assets/images/c-6.jpg" alt="">
                                    <div class="inner-tilt">
                                       <img class="inner-img" src="assets/images/c-6.jpg" alt="">
                                       <div class="slide-cont">
                                          <div>
                                             <h3>MALDIVES
                                             </h3>
                                             <p >Indian Ocean &amp; South East Asia</p>
                                             <span>View itinerary
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section class="main-section testimonial swip">
               <div class="container-fluid">
                  <div class="thm-h text-center">
                     <h2 class="wow fadeInUp" data-wow-duration="0.7s">What Clients Say</h2>
                     <h4>tincidunt eget turpi</h4>
                  </div>
                  <div class="testimonail_outer">
                     <div class="owl-carousel testimonail_slider owl-theme">
                        <div class="item">
                           <div class="testimonail_box">
                              <div class="testimonail_text">
                                 <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse massa odio, eleifend non dictum et, euismod vel mauris. Pellentesque tortor felis, maximus id laoreet a, congue ut diam. Integer volutpat mauris id ligula placerat vestibulum. Nunc laoreet rutrum elit, id fringilla metus. Aliquam mollis blandit arcu scelerisque condimentum. Sed augue nisi pellentesque et tempor.</p>
                              </div>
                              <div class="testimonail_user">
                                 <img src="assets/images/testi_2.jpg">
                                 <span><em>Anderson (Eugenia)</em></span>
                              </div>
                              <div class="testimonial_rating">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="testimonail_box">
                              <div class="testimonail_text">
                                 <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse massa odio, eleifend non dictum et, euismod vel mauris. Pellentesque tortor felis, maximus id laoreet a, congue ut diam. Integer volutpat mauris id ligula placerat vestibulum. Nunc laoreet rutrum elit, id fringilla metus. Aliquam mollis blandit arcu scelerisque condimentum. Sed augue nisi pellentesque et tempor.</p>
                              </div>
                              <div class="testimonail_user">
                                 <img src="assets/images/testi_3.jpg">
                                 <span><em>Anderson (Eugenia)</em></span>
                              </div>
                              <div class="testimonial_rating">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="testimonail_box">
                              <div class="testimonail_text">
                                 <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse massa odio, eleifend non dictum et, euismod vel mauris. Pellentesque tortor felis, maximus id laoreet a, congue ut diam. Integer volutpat mauris id ligula placerat vestibulum. Nunc laoreet rutrum elit, id fringilla metus. Aliquam mollis blandit arcu scelerisque condimentum. Sed augue nisi pellentesque et tempor.</p>
                              </div>
                              <div class="testimonail_user">
                                 <img src="assets/images/testi_1.jpg">
                                 <span><em>Anderson (Eugenia)</em></span>
                              </div>
                              <div class="testimonial_rating">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="testimonail_box">
                              <div class="testimonail_text">
                                 <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse massa odio, eleifend non dictum et, euismod vel mauris. Pellentesque tortor felis, maximus id laoreet a, congue ut diam. Integer volutpat mauris id ligula placerat vestibulum. Nunc laoreet rutrum elit, id fringilla metus. Aliquam mollis blandit arcu scelerisque condimentum. Sed augue nisi pellentesque et tempor.</p>
                              </div>
                              <div class="testimonail_user">
                                 <img src="assets/images/testi_3.jpg">
                                 <span><em>Anderson (Eugenia)</em></span>
                              </div>
                              <div class="testimonial_rating">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="testimonail_box">
                              <div class="testimonail_text">
                                 <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse massa odio, eleifend non dictum et, euismod vel mauris. Pellentesque tortor felis, maximus id laoreet a, congue ut diam. Integer volutpat mauris id ligula placerat vestibulum. Nunc laoreet rutrum elit, id fringilla metus. Aliquam mollis blandit arcu scelerisque condimentum. Sed augue nisi pellentesque et tempor.</p>
                              </div>
                              <div class="testimonail_user">
                                 <img src="assets/images/testi_3.jpg">
                                 <span><em>Anderson (Eugenia)</em></span>
                              </div>
                              <div class="testimonial_rating">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>