<section id="h-charter" class="main-section h-charter-sec swip ">
               <div class="container-fluid">
                  <div class="row m-0 ">
                     <div class="thm-h sec-main-h">
                        <h2 class="wow fadeInUp" data-wow-duration="1s" >Our Team </h2>
                     </div>
                     <div class="col-sm-12"  >
                        <div class="owl-carousel owl-carousel-3 center-carousel owl-theme">
                           <div class="item">
                              <a  href="<?php echo base_url();?>">
                                 <div class="main-tilt">
                                    <div>
                                       <img class="main-back" src="<?php echo base_url();?>assets/images/coaches/deria.png" alt="">
                                    </div>
                                    <div   class="inner-tilt">
                                       <img class="inner-img" src="<?php echo base_url();?>assets/images/coaches/deria.png" alt="">
                                       <div class="slide-cont">
                                          <div>
                                             <h3>Coach Deria
                                             </h3>
                                             <span>CEO
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <div class="item">
                              <a  href="<?php echo base_url();?>">
                                 <div class="main-tilt">
                                    <div>
                                       <img class="main-back" src="<?php echo base_url();?>assets/images/coaches/irfan.png" alt="">
                                    </div>
                                    <div   class="inner-tilt">
                                       <img class="inner-img" src="<?php echo base_url();?>assets/images/coaches/irfan.png" alt="">
                                       <div class="slide-cont">
                                          <div>
                                             <h3>Coach Irfan
                                             </h3>
                                             <span>Coach Manager
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <div class="item">
                              <a  href="<?php echo base_url();?>">
                                 <div class="main-tilt">
                                    <div>
                                       <img class="main-back" src="<?php echo base_url();?>assets/images/coaches/aka.png" alt="">
                                    </div>
                                    <div   class="inner-tilt">
                                       <img class="inner-img" src="<?php echo base_url();?>assets/images/coaches/aka.png" alt="">
                                       <div class="slide-cont">
                                          <div>
                                             <h3>Coach Aka
                                             </h3>
                                             <span>Coach
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <div class="item">
                              <a  href="<?php echo base_url();?>">
                                 <div class="main-tilt">
                                    <div>
                                       <img class="main-back" src="<?php echo base_url();?>assets/images/coaches/danu.png" alt="">
                                    </div>
                                    <div   class="inner-tilt">
                                       <img class="inner-img" src="<?php echo base_url();?>assets/images/coaches/danu.png" alt="">
                                       <div class="slide-cont">
                                          <div>
                                             <h3>Coach Danu
                                             </h3>
                                             <span>Coach
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                            <div class="item">
                                <a  href="<?php echo base_url();?>">
                                    <div class="main-tilt">
                                        <div>
                                            <img class="main-back" src="<?php echo base_url();?>assets/images/coaches/murdi.png" alt="">
                                        </div>
                                        <div   class="inner-tilt">
                                            <img class="inner-img" src="<?php echo base_url();?>assets/images/coaches/murdi.png" alt="">
                                            <div class="slide-cont">
                                                <div>
                                                    <h3>Coach Murdi
                                                    </h3>
                                                    <span>Coach
                                             </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>