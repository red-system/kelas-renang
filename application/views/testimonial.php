<section class="main-section testimonial swip">
               <div class="container-fluid">
                  <div class="thm-h text-center">
                     <h2 class="wow fadeInUp" data-wow-duration="0.7s">What Clients Say</h2>
                     <h4>Our Statisfied Clients</h4>
                  </div>
                  <div class="testimonail_outer">
                     <div class="owl-carousel testimonail_slider owl-theme">
                        <div class="item">
                           <div class="testimonail_box">
                              <div class="testimonail_text">
                                 <p> Tepat Waktu, mengajar dengan sepenuh hati. Memberikan apa yan dibutuhkan oleh yang dilatih, terutama memberikan masukan masukan tentang gerakan yan keliru. Saran-saran yan diberikan juga sanat baik sehingga saya bisa memahami kekuranan saya.</p>
                              </div>
                              <div class="testimonail_user">
                                 <img src="<?php echo base_url();?>assets/images/kelas-renang-testimoni/1.jpeg">
                                 <span><em>Yakub</em></span>
                              </div>
                              <div class="testimonial_rating">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="testimonail_box">
                              <div class="testimonail_text">
                                 <p> Sangat menyenangkan, waktu dan tempat fleksibel adi sangat menyenankan. Coach-nya sanat baik, bikin Nathan semangat belajar renang. </p>
                              </div>
                              <div class="testimonail_user">
                                 <img src="<?php echo base_url();?>assets/images/kelas-renang-testimoni/2.jpeg">
                                 <span><em>Nathan's Mom</em></span>
                              </div>
                              <div class="testimonial_rating">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="testimonail_box">
                              <div class="testimonail_text">
                                 <p> Thank You So Much!. Sekaran saya dan Chaitylyn sudah bisa renang bareng di Kolam. Walaupun baru cuma 1 sesi dan harus ditemani, tapi sudah bisa berenang tanpa pelampung. Dia bilang, "aku mau lanjut les berenang biar bisa sama Miss Deria yang Baik". Ms Deria juga sabar banget megang Chitylyn yang kadang Moodie karena baru pulang sekolah. Pelatih pun ngebujuk dengan mainan kesukaan Chaitylyn sehinga mau melanjutkan lesnya dengan ceria. Recommended banget pokoknya terimakasih <a href="https://www.kelasrenang.id"><i>kelasrenang.id</i></a>, terutama pelatihnya yang semua profesional!</p>
                              </div>
                              <div class="testimonail_user">
                                 <img src="<?php echo base_url();?>assets/images/kelas-renang-testimoni/3.jpeg">
                                 <span><em>Caithylyn's Mom</em></span>
                              </div>
                              <div class="testimonial_rating">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </div>
                         <div class="item">
                             <div class="testimonail_box">
                                 <div class="testimonail_text">
                                     <p> Makasih ya sudah ngajarin aku renang dengan baik, aku ga nyangka bisa bisa dengan cepat dan akan aku rekomendasiin ke temen-temen. Pengajarnya baik dan profesional, Good Job ! <a href="https://www.kelasrenang.id"><i>kelasrenang.id</i></a></p>
                                 </div>
                                 <div class="testimonail_user">
                                     <img src="<?php echo base_url();?>assets/images/kelas-renang-testimoni/4.jpeg">
                                     <span><em>Wira</em></span>
                                 </div>
                                 <div class="testimonial_rating">
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                 </div>
                             </div>
                         </div>
                         <div class="item">
                             <div class="testimonail_box">
                                 <div class="testimonail_text">
                                     <p> Keren, diajarin sama coach nya bikin deg-degan sama air nya hilang seketika. good job deh pokoknya!</p>
                                 </div>
                                 <div class="testimonail_user">
                                     <img src="<?php echo base_url();?>assets/images/kelas-renang-testimoni/5.jpeg">
                                     <span><em>Dewi</em></span>
                                 </div>
                                 <div class="testimonial_rating">
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                 </div>
                             </div>
                         </div>
<!--                         <div class="item">-->
<!--                             <div class="testimonail_box">-->
<!--                                 <div class="testimonail_text">-->
<!--                                     <p> Aqua Prenatal is one of the most interesting swimming class that i've ever join in <a href="https://www.kelasrenang.id"><i>kelasrenang.id</i></a>. The coaches are friendly and was able to teach me even before the class is over.</p>-->
<!--                                 </div>-->
<!--                                 <div class="testimonail_user">-->
<!--                                     <img src="--><?php //echo base_url();?><!--assets/images/kelas-renang-testimoni/6.jpeg">-->
<!--                                     <span><em>Mhita</em></span>-->
<!--                                 </div>-->
<!--                                 <div class="testimonial_rating">-->
<!--                                     <i class="fas fa-star"></i>-->
<!--                                     <i class="fas fa-star"></i>-->
<!--                                     <i class="fas fa-star"></i>-->
<!--                                     <i class="fas fa-star"></i>-->
<!--                                     <i class="fas fa-star"></i>-->
<!--                                 </div>-->
<!--                             </div>-->
<!--                         </div>-->
                         <div class="item">
                             <div class="testimonail_box">
                                 <div class="testimonail_text">
                                     <p>Sangat menyenangkan menurut saya, karena pelayananya baik dan memberikan materi dan arahan juga baik dan saya sendiripun juga bisa cepat tangap dalam renang karena bimbinan dari <a href="https://www.kelasrenang.id"><i>kelasrenang.id</i></a>. Untuk saran tetaplah selalu berikan pelayanan terbaik dan memuaskan seperti ini.</p>
                                 </div>
                                 <div class="testimonail_user">
                                     <img src="<?php echo base_url();?>assets/images/kelas-renang-testimoni/7.jpeg">
                                     <span><em>Aldi Pratama</em></span>
                                 </div>
                                 <div class="testimonial_rating">
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                 </div>
                             </div>
                         </div>
                         <div class="item">
                             <div class="testimonail_box">
                                 <div class="testimonail_text">
                                     <p>Sebelumnya makasih banyak diajarin dari sampai bisa, untuk pelatihnya rapmah dan mudah dimengerti. makasih banyak kelas renang sudah menggajari saya siap untuk tes renang syarat masuk tentara. </p>
                                 </div>
                                 <div class="testimonail_user">
                                     <img src="<?php echo base_url();?>assets/images/kelas-renang-testimoni/8.jpeg">
                                     <span><em>Roy</em></span>
                                 </div>
                                 <div class="testimonial_rating">
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                 </div>
                             </div>
                         </div>
                         <div class="item">
                             <div class="testimonail_box">
                                 <div class="testimonail_text">
                                     <p>Renan di <a href="https://www.kelasrenang.id"><i>kelasrenang.id</i></a> enaaaaaaaak bangeeet . . .waktunya fleksibel, Ms Yani juga sabar dan sayang banget pada anak apalagi Alvero kadang suka Cranky tapi ada aja caranya supaya mau renang. Hehehe , Alhamdulilah sih jadinya ga takut air lagi . . Seneng diajak renang</p>
                                 </div>
                                 <div class="testimonail_user">
                                     <img src="<?php echo base_url();?>assets/images/kelas-renang-testimoni/9.jpeg">
                                     <span><em>Fetty</em></span>
                                 </div>
                                 <div class="testimonial_rating">
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                     <i class="fas fa-star"></i>
                                 </div>
                             </div>
                         </div>

                        </div>
                     </div>
                  </div>
               </div>
            </section>