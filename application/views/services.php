<section class="main-section our-services swip">
               <div class="container-fluid">
                  <div class="thm-h text-center">
                     <h2 class="wow fadeInUp" data-wow-duration="0.7s">Program <a href="https://www.kelasrenang.id"><i>kelasrenang.id</i></a></h2>
                     <h4>Tenaga pengajar berpengalaman dan tersertifikasi</h4>
                  </div>
                  <div class="row m-0">
                     <div class="col-md-3">
                        <div class="servicebox-first" style="background-image:url(<?php echo base_url();?>assets/images/kelas-renang-training.jpg)">
                           <div class="thm-h text-center">
                              <h3 class="wow fadeInUp" data-wow-duration="0.7s" style="color:white;"><a href="https://www.kelasrenang.id"><i>kelasrenang.id</i></a></h3>
                              <h4>Program Khusus Sesuai Kebutuhan</h4>
                              <br>
                              <p>kami sangat mengutamakan kenyamanan pada saat belajar terutama untuk masyrakat yang mempunyai trauma ataupun fobia dengan air. </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-9">
                        <div class="row servicebox-row">
                           <div class="col-md-4">
                              <div class="servicebox">
                                 <img src="<?php echo base_url();?>assets/images/program-kelas-renang/aqua-prenatal-yoga.png">
                                 <h4>Aqua Prenatal Yoga</h4>
                                 <p>Aqua prenatal yoga adalah program yoga hamil yang kami modifikasi dengan media air.</p>
                                 <!-- <a class="servicemore" href="home-1.html#">More</a> -->
                              </div>
                           </div>
                            <div class="col-md-4">
                                <div class="servicebox">
                                    <img src="<?php echo base_url();?>assets/images/program-kelas-renang/prenatal-swimming2.png">
                                    <h4>Prenatal Swimming</h4>
                                    <p>Prenatal Swimming adalah kelas yang ditujukan kepada Ibu Hamil yang ingin merasakan sensasi relaksasi pada saat masa kehamilan.</p>
                                    <!-- <a class="servicemore" href="home-1.html#">More</a> -->
                                </div>
                            </div>
                           <div class="col-md-4">
                              <div class="servicebox">
                                 <img src="<?php echo base_url();?>assets/images/program-kelas-renang/kelas-bayi.png">
                                 <h4>Baby Class</h4>
                                 <p>Renang pada bayi mempunyai banyak sekali manfaat nya.</p>
                                 <!-- <a class="servicemore" href="home-1.html#">More</a> -->
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="servicebox">
                                 <img src="<?php echo base_url();?>assets/images/program-kelas-renang/kelas-toddler.png">
                                 <h4>Toddler Class</h4>
                                 <p>Kelas toddler dapat di ikuti oleh anak usia 2,5y hingga 5y.</p>
                                 <!-- <a class="servicemore" href="home-1.html#">More</a> -->
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="servicebox">
                                 <img src="<?php echo base_url();?>assets/images/program-kelas-renang/kelas-kids.png">
                                 <h4>Kid's Class</h4>
                                 <p>Dapat di ikuti oleh anak usia >5y - 15y.</p>
                                 <!-- <a class="servicemore" href="home-1.html#">More</a> -->
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="servicebox">
                                 <img src="<?php echo base_url();?>assets/images/program-kelas-renang/kelas-teens.png">
                                 <h4>Teen Class</h4>
                                 <p>Dapat diikuti oleh anak usia > 17y hingga >20y.</p>
                                 <!-- <a class="servicemore" href="home-1.html#">More</a> -->
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="servicebox">
                                 <img src="<?php echo base_url();?>assets/images/program-kelas-renang/kelas-dewasa.png">
                                 <h4>Adult Class</h4>
                                 <p>Dapat diikuti oleh dewasa >20y</p>
                                 <!-- <a class="servicemore" href="home-1.html#">More</a> -->
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="servicebox">
                                 <img src="<?php echo base_url();?>assets/images/program-kelas-renang/kelas-terapi-berkebutuhan-khusus.png">
                                 <h4>Special Needs and Therapy Class</h4>
                                 <p>Fokus pada motorik kasar.</p>
                                 <!-- <a class="servicemore" href="home-1.html#">More</a> -->
                              </div>
                           </div>
                            <div class="col-md-4">
                                <div class="servicebox">
                                    <img src="<?php echo base_url();?>assets/images/program-kelas-renang/intensive-class.png">
                                    <h4>Intensive Class</h4>
                                    <p>Persiapan cepat menghadapi seleksi masuk instansi yang memprasyaratkan Kemampuan Renang.</p>
                                    <!-- <a class="servicemore" href="home-1.html#">More</a> -->
                                </div>
                            </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
